# 3d Pec01

Juego de coches realizado para la PEC03.

## Como jugar

### Para este juego tiene que utilizar cuatro botones:

- A -> Ir hacia la izquierda
- D -> Ir hacia la derecha
- W -> Ir hacia arriba
- S -> Ir hacia abajo / frenar
- Space -> Frenar

### Pasos para comenzar el juego:

1. Iniciar el ejecutable del juego.

2. Cuando se inicie el juego estara en la escena Menu donde tiene que pulsar encima del texto para empezar a jugar.

3. Debera pasarse el circuito y llegar a superar las tres vueltas.

4. Si cae puede presionar en reintentar para volver a empezar la carrera.

## Video

[Enlace al video](https://youtu.be/vZgI5ORdW8E "Enlace al video")

## Instaladores

- [ ] [Windows 10](https://drive.google.com/file/d/1MUmIIsmMuuI98ji0yBzwpOZ2USRHh5Rv/view?usp=sharing)
- [ ] [Mac OS](https://drive.google.com/file/d/1iIWTJgnVFY6RdZz7yj1-oYmgGpyhbOVs/view?usp=sharing)

## Partes importantes del codigo

#### Estructura del proyecto

- Assets
 - Scenes: En esta carpeta se encuentra las escenas del juego (Menu, Pista01 y Pista02).
 - Scripts: Se encuentra los scripts de todo el juego.
   - Coches: El script que controla el coche.
   - Menus: Todo lo relacionado con los menus y UI.
   - Pistas: Eventos de la carrera y gestores de la misma.
 - Tiles: Contiene todas las imagenes y tiles del juego.
   - Personajes: Contiene los tiles del protagonista y de los enemigos.
   - Escenario

#### Coches

**Archivo: Assets/Scripts/Coches/Car.cs**

En el update comprobamos si es el coche fantasma o con el que estamos jugando, el coche fantasma tambien se utiliza para reproducir la repeticion.

```csharp
    void Update()
    {
        if (enable && !fantasma)
        {
            int velocidad = (int)(rigid.velocity.magnitude / 0.1f);
            EventoPista.actualizarVelocidad(velocidad);
            if(velocidad < 20)
            {
                if(!stop.isPlaying)
                    stop.Play();
                if (run.isPlaying)
                    run.Stop();
            }else if(velocidad < 100)
            {
                if (stop.isPlaying)
                    stop.Stop();
                if (!run.isPlaying)
                    run.Play();
                run.volume = 0.50f;
            }
            else if (velocidad < 180)
            {
                if (!run.isPlaying)
                    run.Play();
                run.volume = 0.70f;
            }
            else if (velocidad < 280)
            {
                if (!run.isPlaying)
                    run.Play();
                run.volume = 1.0f;
            }
        }
        else if(enable && fantasma)
        {
            moverFantasma();
        }
    }
```

Para mover el fantasma interpolamos entre las posiciones guardadas en memoria (recuperadas de la pista guardada o de la carrera actual), cada cierto tiempo se aumenta el indice para recuperar la posicion.

```csharp
    /// <summary>
    /// interpola al coche fantasma
    /// </summary>
    private void moverFantasma()
    {
        if(pasoRecorridoFantasma < recorridoFantasma.Length)
        {
            this.transform.position = Vector3.Slerp(this.transform.position, new Vector3(recorridoFantasma[pasoRecorridoFantasma].x, recorridoFantasma[pasoRecorridoFantasma].y, recorridoFantasma[pasoRecorridoFantasma].z), 0.1f);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, new Quaternion(recorridoFantasma[pasoRecorridoFantasma].rx, recorridoFantasma[pasoRecorridoFantasma].ry, recorridoFantasma[pasoRecorridoFantasma].rz, recorridoFantasma[pasoRecorridoFantasma].rw), 0.1f);
        }
    }
```

Para procesar el coche normal recogemos los inputs en "recogerInputs" y los procesamos en el metodo "procesarInputs".

```csharp
    /// <summary>
    /// Recogemos los inputs
    /// </summary>
    private void recogerInputs()
    {
        // Recogemos del Input los valores de aceleración,giro y freno. 
        motor = maxMotorTorque * Input.GetAxis("Vertical");
        steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        brake = maxBrakeTorque * Input.GetAxis("Jump");
    }

    /// <summary>
    /// procesa los inputs y determina que acciones tomar segun sus valores
    /// </summary>
    private void procesarInputs()
    {
        Vector3 position;
        Quaternion rotation;
        for (int a = 0; a < colliderRuedas.Length; a++)
        {
            colliderRuedas[a].steerAngle = steering;
            colliderRuedas[a].motorTorque = motor;
            colliderRuedas[a].brakeTorque = brake;
            colliderRuedas[a].GetWorldPose(out position, out rotation);
            objetosRuedas[a].transform.position = position;
            objetosRuedas[a].transform.rotation = rotation;
        }
    }
```

#### Pistas

**Archivo: Assets/Scripts/Pistas/ControladorPista.cs**

Cuando inicializamos definimos los eventos que pueden ser llamados de forma externa, instanciamos el fantasma si existe y llamamos a la cuenta atras.

```csharp
    void inicializar()
    {

        inicializado = true;

        EventoPista.actualizarVelocidad = actualizarVelocidad;
        EventoPista.comprobadorVuelta = comprobacionVuelta;
        EventoPista.metaVuelta = meta;
        EventoPista.perder = gameOver;
        EventoPista.reproducir = reproducir;

        ControladorRecorder.cargarCarrera();

        if(PartidaEventos.partida != null && PartidaEventos.partida.tiempo > 0)
        {
            //si que hay fantasma
            instanciarFantasma();
        }
        else
        {
            //no hay fantasma
            PartidaEventos.partida = new Partida();
            PartidaEventos.partida.tiempo = 0;
        }

        activarPanel(1); // activamos ui correr
        Invoke("iniciar",1); // fase ready

    }
```

Actualizamos los paneles vinculados al gameobject.

```csharp
    /// <summary>
    /// Actualiza el marcador de velocidad
    /// </summary>
    /// <param name="speed"></param>
    public void actualizarVelocidad(int speed)
    {
        this.velocidad.text = speed+"";
    }

    /// <summary>
    /// Actualiza el contador de vueltas
    /// </summary>
    public void actualizarVueltas()
    {
        this.vueltas.text = (numVueltasRealizadas+1) + "/" + EventoPista.VUELTAS;
    }

```

**Archivo: Assets/Scripts/Pistas/EventoPista.cs**

En esta clase almacenamos casi todos los eventos externos y las constantes importantes del juego.

```csharp
    /// <summary>
    /// Indica las vueltas de la partida
    /// </summary>
    public static int TIEMPOMAXIMO = 500;

    /// <summary>
    /// Indica las vueltas de la partida
    /// </summary>
    public static int VUELTAS = 3;

    /// <summary>
    /// Indica la pista con la que se juega
    /// </summary>
    public static string PISTA = "pista01";

    /// <summary>
    /// Indica el coche con el que se juega
    /// </summary>
    public static int COCHE = 1;

    /// <summary>
    /// Actualiza el texto de la velocidad del coche 
    /// </summary>
    public static Action<int> actualizarVelocidad;

    /// <summary>
    /// Realiza acciones cuando el jugador pasa por la comprobacion de vuelta
    /// </summary>
    public static Action comprobadorVuelta;

    /// <summary>
    /// Realiza acciones cuando el jugador llega a la meta
    /// </summary>
    public static Action metaVuelta;

    /// <summary>
    /// Realiza acciones cuando el jugador pierde
    /// </summary>
    public static Action perder;

    /// <summary>
    /// Reproduce el ultimo recorrido
    /// </summary>
    public static Action reproducir;
```


#### Menus

**Archivo: Assets/Scripts/Menus/ControladorMenu.cs**

Definimos los comportamientos de los panales para que se habiliten y se deshabiliten, estos los vinculamos con sus eventos (Actions).

```csharp
    /// <summary>
    ///  Vuelve al menu
    /// </summary>
    public void volverMenu()
    {
        coches.SetActive(false);
        menu.SetActive(true);
        pistas.SetActive(false);
    }

    /// <summary>
    ///  Vuelve al menu de coches
    /// </summary>
    public void avanzarCoches()
    {
        coches.SetActive(true);
        menu.SetActive(false);
        pistas.SetActive(false);
    }

    /// <summary>
    ///  Vuelve al menu de pistas
    /// </summary>
    public void avanzarPistas()
    {
        coches.SetActive(false);
        menu.SetActive(false);
        pistas.SetActive(true);
    }
```

## Creditos

Creditos de las diferentes obras que se han utilizado:

### Sonidos

- Musica del menu 
Autor: **Mrpoly**
Enlace: OpenGameArts

- Musica de los niveles
Autor: **Rezoner**
Enlace: OpenGameArts

- Musica acelerar
Autor: **SubspaceAudio**
Enlace: OpenGameArts

### Tiles, Imagenes

- Coches
Autor: **GameArt2D** - **Zuhria Alfitra**
Enlace: OpenGameArts

- Imagen menu
Autor: **Clint Bellanger**
Enlace: OpenGameArts