using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Point {

    /// <summary>
    /// Posiciones
    /// </summary>
    public float x,y,z;

    /// <summary>
    /// Rotaciones
    /// </summary>
    public float rx,ry,rz,rw;

    /// <summary>
    /// Constructor basico del punto
    /// </summary>
    public Point()
    {
        x = 0;
        y = 0;
        z = 0;
        rx = 0;
        ry = 0;
        rz = 0;
        rw = 0;
    }

}
