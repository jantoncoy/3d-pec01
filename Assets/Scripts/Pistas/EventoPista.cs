using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contiene los eventos de la pista
/// </summary>
public static class EventoPista
{

    /// <summary>
    /// Indica las vueltas de la partida
    /// </summary>
    public static int TIEMPOMAXIMO = 500;

    /// <summary>
    /// Indica las vueltas de la partida
    /// </summary>
    public static int VUELTAS = 3;

    /// <summary>
    /// Indica la pista con la que se juega
    /// </summary>
    public static string PISTA = "pista01";

    /// <summary>
    /// Indica el coche con el que se juega
    /// </summary>
    public static int COCHE = 1;

    /// <summary>
    /// Actualiza el texto de la velocidad del coche 
    /// </summary>
    public static Action<int> actualizarVelocidad;

    /// <summary>
    /// Realiza acciones cuando el jugador pasa por la comprobacion de vuelta
    /// </summary>
    public static Action comprobadorVuelta;

    /// <summary>
    /// Realiza acciones cuando el jugador llega a la meta
    /// </summary>
    public static Action metaVuelta;

    /// <summary>
    /// Realiza acciones cuando el jugador pierde
    /// </summary>
    public static Action perder;

    /// <summary>
    /// Reproduce el ultimo recorrido
    /// </summary>
    public static Action reproducir;
}
