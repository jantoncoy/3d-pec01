using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;

/// <summary>
/// Se encuentra lo necesario para correr en una pista
/// </summary>
public class ControladorPista : MonoBehaviour
{
    /// <summary>
    /// Gameobject del coche
    /// </summary>
    public GameObject car,fantasma;

    /// <summary>
    /// Contiene el texto de la velocidad
    /// </summary>
    public TextMeshProUGUI velocidad;


    /// <summary>
    /// Contiene el texto de inicio
    /// </summary>
    public TextMeshProUGUI start;


    /// <summary>
    /// Contiene el texto del tiempo
    /// </summary>
    public TextMeshProUGUI tiempo;

    /// <summary>
    /// Contiene el texto de las vueltas
    /// </summary>
    public TextMeshProUGUI vueltas;

    /// <summary>
    /// Punto respawn pista
    /// </summary>
    public Transform respawn;

    /// <summary>
    /// Paneles del ui
    /// </summary>
    public GameObject perder, ganar, rec, correr;

    /// <summary>
    /// Coches
    /// </summary>
    public GameObject [] prefabs;

    /// <summary>
    /// Camaras
    /// </summary>
    public GameObject [] camaras;

    /// <summary>
    /// Camaras
    /// </summary>
    public GameObject[] camarasVirtuales;

    /// <summary>
    /// Booleanos necesarios
    /// </summary>
    private bool inicializado = false, comprobacion = false, contar = false;

    /// <summary>
    /// Integers necesarios
    /// </summary>
    private int numVueltasRealizadas = 0, segundosRealizados = 0;

    // Start is called before the first frame update
    void Start()
    {
        inicializado = false;
        comprobacion = false;
        numVueltasRealizadas = 0;
        segundosRealizados = 0;

        instanciarObjetos();
    }

    /// <summary>
    /// Instancia los objetos necesarios del fantasma
    /// </summary>
    private void instanciarFantasma()
    {
        fantasma = Instantiate(prefabs[PartidaEventos.partida.coche - 1], respawn.position, respawn.rotation);
        fantasma.GetComponent<BoxCollider>().isTrigger = true;
        fantasma.GetComponent<Car>().fantasma = true;
        fantasma.GetComponent<Car>().recorridoFantasma = PartidaEventos.partida.points.ToArray();
        fantasma.GetComponent<Car>().convertirEnFantasma();
        Destroy(fantasma.GetComponent<Rigidbody>());
    }

    /// <summary>
    /// Instancia los objetos necesarios del fantasma
    /// </summary>
    private void instanciarReproduccion()
    {
        fantasma = Instantiate(prefabs[EventoPista.COCHE - 1], respawn.position, respawn.rotation);
        fantasma.GetComponent<BoxCollider>().isTrigger = true;
        fantasma.GetComponent<Car>().fantasma = true;
        fantasma.GetComponent<Car>().recorridoFantasma = ControladorRecorder.grabacion.ToArray();
        Destroy(fantasma.GetComponent<Rigidbody>());
        fantasma.SetActive(true);
    }

    /// <summary>
    /// Instancia y vincula las camaras para el recorrido del coche
    /// </summary>
    private void instanciarObjetos()
    {
        car = Instantiate(prefabs[EventoPista.COCHE - 1], respawn.position, respawn.rotation);

        foreach (GameObject cam in camarasVirtuales)
        {
            cam.GetComponent<CinemachineVirtualCamera>().LookAt = car.transform;
            cam.GetComponent<CinemachineVirtualCamera>().Follow = car.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!inicializado)
        {
            inicializar();
        }
    }

    /// <summary>
    /// Agregamos funciones callback de inicializacion
    /// </summary>
    void inicializar()
    {

        inicializado = true;

        EventoPista.actualizarVelocidad = actualizarVelocidad;
        EventoPista.comprobadorVuelta = comprobacionVuelta;
        EventoPista.metaVuelta = meta;
        EventoPista.perder = gameOver;
        EventoPista.reproducir = reproducir;

        ControladorRecorder.cargarCarrera();

        if(PartidaEventos.partida != null && PartidaEventos.partida.tiempo > 0)
        {
            //si que hay fantasma
            instanciarFantasma();
        }
        else
        {
            //no hay fantasma
            PartidaEventos.partida = new Partida();
            PartidaEventos.partida.tiempo = 0;
        }

        activarPanel(1); // activamos ui correr
        Invoke("iniciar",1); // fase ready

    }

    /// <summary>
    /// Actualiza el marcador de velocidad
    /// </summary>
    /// <param name="speed"></param>
    public void actualizarVelocidad(int speed)
    {
        this.velocidad.text = speed+"";
    }

    /// <summary>
    /// Actualiza el contador de vueltas
    /// </summary>
    public void actualizarVueltas()
    {
        this.vueltas.text = (numVueltasRealizadas+1) + "/" + EventoPista.VUELTAS;
    }

    /// <summary>
    /// Pone la comprobacion de la meta a true
    /// </summary>
    public void comprobacionVuelta()
    {
        comprobacion = true;
    }

    /// <summary>
    /// Salta cuando se pasa por la meta
    /// </summary>
    public void meta()
    {
        if (comprobacion)
        {
            //se suma vuelta
            if(EventoPista.VUELTAS <= (numVueltasRealizadas+1))
            {
                //se termina
                car.GetComponent<Car>().enable = false;
                contar = false;

                if((EventoPista.TIEMPOMAXIMO - segundosRealizados) > PartidaEventos.partida.tiempo)
                {
                    //es mayor
                    ControladorRecorder.guardarCarrera((EventoPista.TIEMPOMAXIMO - segundosRealizados));
                    activarPanel(3);
                }
                else
                {
                    //es menor
                    gameOver();
                }
            }
            else
            {
                //se sigue
                numVueltasRealizadas++;
                actualizarVueltas();
            }

            comprobacion = false;
        }
    }

    /// <summary>
    /// Hace perder al jugador
    /// </summary>
    public void gameOver()
    {
        activarPanel(4);
        car.GetComponent<Car>().enable = false;
        contar = false;
    }

    /// <summary>
    /// Activa un panel y desactiva los otros
    /// 1 - correr
    /// 2 - grabar
    /// 3 - ganar
    /// 4 - perder
    /// </summary>
    /// <param name="panel"></param>
    private void activarPanel(int panel)
    {
        if (panel == 1)
            correr.SetActive(true);
        else
            correr.SetActive(false);

        if (panel == 2)
            rec.SetActive(true);
        else
            rec.SetActive(false);

        if (panel == 3)
            ganar.SetActive(true);
        else
            ganar.SetActive(false);

        if (panel == 4)
            perder.SetActive(true);
        else
            perder.SetActive(false);
    }

    /// <summary>
    /// Activa una camara y desactiva las otras
    /// 0 - persigue al jugador
    /// 1 - derecha
    /// 2 - izquierda
    /// 3 - frente
    /// </summary>
    /// <param name="camara"></param>
    private void activarCamara(int camara)
    {
        for(int a = 0; a < camaras.Length;a++)
        {
            if (a == camara)
                camaras[a].SetActive(true);
            else
                camaras[a].SetActive(false);
        }
    }

    /// <summary>
    /// Cuenta atras
    /// </summary>
    private void iniciar()
    {
        Invoke("three", 2); // fase ready
    }

    /// <summary>
    /// Cuenta atras
    /// </summary>
    private void one()
    {
        start.text = 1 + "";
        Invoke("go", 1); // fase ready
    }

    /// <summary>
    /// Cuenta atras
    /// </summary>
    private void two()
    {
        start.text = 2 + "";
        Invoke("one", 1); // fase ready
    }

    /// <summary>
    /// Cuenta atras
    /// </summary>
    private void three()
    {
        start.text = 3+"";
        Invoke("two", 1); // fase ready
    }

    /// <summary>
    /// Inicia la carrera
    /// </summary>
    private void go()
    {
        if (PartidaEventos.partida != null && PartidaEventos.partida.tiempo > 0)
        {
            fantasma.GetComponent<Car>().enable = true;
        }
        contar = true;
        car.GetComponent<Car>().enable = true;
        start.text = "";
        InvokeRepeating("sumarTiempo", 1, 1f);
    }

    /// <summary>
    /// Incrementa el tiempo de la carrera
    /// </summary>
    private void sumarTiempo()
    {
        if (contar)
        {
            segundosRealizados += 1;
            tiempo.text = (EventoPista.TIEMPOMAXIMO - segundosRealizados) + "";
        }
    }

    /// <summary>
    /// Reproduce el ultimo recorrido guardado
    /// </summary>
    private void reproducir()
    {
        if(car != null)
            car.SetActive(false);
        if(fantasma != null)
            fantasma.SetActive(false);

        activarPanel(2);

        instanciarReproduccion();

        foreach (GameObject cam in camarasVirtuales)
        {
            cam.GetComponent<CinemachineVirtualCamera>().LookAt = fantasma.transform;
            cam.GetComponent<CinemachineVirtualCamera>().Follow = fantasma.transform;
        }

        activarCamara(2);

        fantasma.GetComponent<Car>().enable = true; //activamos el coche

        InvokeRepeating("cambiarCamaraReproduccion",3f,4f);
    }

    /// <summary>
    /// Va rotondo de forma aleatoria por las camaras
    /// </summary>
    private void cambiarCamaraReproduccion()
    {
        activarCamara(Random.Range(1, 4));
    }

}
