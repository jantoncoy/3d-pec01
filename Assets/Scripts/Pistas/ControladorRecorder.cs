using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorRecorder : MonoBehaviour
{
    /// <summary>
    /// Grabamos un punto
    /// </summary>
    public static Action<Transform> grabarPunto;

    /// <summary>
    /// Guardamos fantasma
    /// </summary>
    public static Action<int> guardarCarrera;

    /// <summary>
    /// Cargamos fantasma
    /// </summary>
    public static Action cargarCarrera;

    /// <summary>
    /// Fantamos actual
    /// </summary>
    public static List<Point> fantasma;

    /// <summary>
    /// Reproducion actual
    /// </summary>
    public static List<Point> grabacion;

    // Start is called before the first frame update
    void Start()
    {
        ControladorRecorder.grabacion = new List<Point>();
        ControladorRecorder.grabarPunto = grabar;
        ControladorRecorder.guardarCarrera = guardar;
        ControladorRecorder.cargarCarrera = cargar;
    }

    public void guardar(int tiempo)
    {
        Partida nuevaPartida = new Partida();
        nuevaPartida.coche = EventoPista.COCHE;
        nuevaPartida.pista = EventoPista.PISTA;
        nuevaPartida.tiempo = tiempo;
        nuevaPartida.points = grabacion;
        PartidaEventos.partida = nuevaPartida;
        PartidaEventos.guardar();
    }

    public void cargar()
    {
        //Cargamos el fantasma anterior
        PartidaEventos.cargar();
        ControladorRecorder.fantasma = PartidaEventos.partida.points;
    }

    public void grabar(Transform punto)
    {
        if (ControladorRecorder.grabacion == null)
            ControladorRecorder.grabacion = new List<Point>();

        Point nuevoPunto = new Point();
        nuevoPunto.x = punto.position.x;
        nuevoPunto.y = punto.position.y;
        nuevoPunto.z = punto.position.z;
        nuevoPunto.rx = punto.rotation.x;
        nuevoPunto.ry = punto.rotation.y;
        nuevoPunto.rz = punto.rotation.z;
        nuevoPunto.rw = punto.rotation.w;

        ControladorRecorder.grabacion.Add(nuevoPunto);
    }
}
