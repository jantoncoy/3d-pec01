using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Partida {

    /// <summary>
    /// Pista actual
    /// </summary>
    public string pista;

    /// <summary>
    /// Tiempo de carrera
    /// </summary>
    public int tiempo;

    /// <summary>
    /// Coche de la carrera
    /// </summary>
    public int coche;

    /// <summary>
    /// Puntos de posicion y rotacion
    /// </summary>
    public List<Point> points;

    /// <summary>
    /// Constructor basico de la partida
    /// </summary>
    public Partida()
    {
        pista = "";
        tiempo = -1;
        coche = -1;
        points = new List<Point>();
    }

}
