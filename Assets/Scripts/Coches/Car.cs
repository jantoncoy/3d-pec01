using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contiene toda la logica necesaria para mover el coche
/// </summary>
public class Car : MonoBehaviour
{
    /// <summary>
    /// WheelColliders para las ruedas
    /// </summary>
    public WheelCollider [] colliderRuedas;

    /// <summary>
    /// Objetos de rueda
    /// </summary>
    public GameObject [] objetosRuedas;

    /// <summary>
    /// Maximo giro
    /// </summary>
    public float maxSteeringAngle;

    /// <summary>
    /// Maxima fuerza del motor
    /// </summary>
    public float maxMotorTorque;

    /// <summary>
    /// Maxima fuerza de frenada
    /// </summary>
    public float maxBrakeTorque;

    /// <summary>
    /// Fuerza de motor
    /// </summary>
    private float motor;

    /// <summary>
    /// Direccion
    /// </summary>
    private float steering;

    /// <summary>
    /// Frenada
    /// </summary>
    private float brake;

    /// <summary>
    /// Control enable or disable
    /// </summary>
    public bool enable = false,fantasma=false;

    /// <summary>
    /// Rigidbody del coche
    /// </summary>
    private Rigidbody rigid;

    /// <summary>
    /// Contiene todas las mallas con texturas
    /// </summary>
    public MeshRenderer[] mallas;

    /// <summary>
    /// Contiene el recorrido del fantasma
    /// </summary>
    public Point [] recorridoFantasma = null;

    /// <summary>
    /// Paso actual del fantasma
    /// </summary>
    int pasoRecorridoFantasma = 0;

    /// <summary>
    /// Audios del motor
    /// </summary>
    public AudioSource stop, run;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("grabarPunto",0.1f,0.1f);
        rigid = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enable && !fantasma)
        {
            int velocidad = (int)(rigid.velocity.magnitude / 0.1f);
            EventoPista.actualizarVelocidad(velocidad);
            if(velocidad < 20)
            {
                if(!stop.isPlaying)
                    stop.Play();
                if (run.isPlaying)
                    run.Stop();
            }else if(velocidad < 100)
            {
                if (stop.isPlaying)
                    stop.Stop();
                if (!run.isPlaying)
                    run.Play();
                run.volume = 0.50f;
            }
            else if (velocidad < 180)
            {
                if (!run.isPlaying)
                    run.Play();
                run.volume = 0.70f;
            }
            else if (velocidad < 280)
            {
                if (!run.isPlaying)
                    run.Play();
                run.volume = 1.0f;
            }
        }
        else if(enable && fantasma)
        {
            moverFantasma();
        }
    }

    /// <summary>
    /// interpola al coche fantasma
    /// </summary>
    private void moverFantasma()
    {
        if(pasoRecorridoFantasma < recorridoFantasma.Length)
        {
            this.transform.position = Vector3.Slerp(this.transform.position, new Vector3(recorridoFantasma[pasoRecorridoFantasma].x, recorridoFantasma[pasoRecorridoFantasma].y, recorridoFantasma[pasoRecorridoFantasma].z), 0.1f);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, new Quaternion(recorridoFantasma[pasoRecorridoFantasma].rx, recorridoFantasma[pasoRecorridoFantasma].ry, recorridoFantasma[pasoRecorridoFantasma].rz, recorridoFantasma[pasoRecorridoFantasma].rw), 0.1f);
        }
    }

    /// <summary>
    /// Graba un punto de la carrera
    /// </summary>
    public void grabarPunto()
    {
        if (enable && !fantasma)
        {
            ControladorRecorder.grabarPunto(this.transform);
        }
        else if (enable && fantasma)
        {
            pasoRecorridoFantasma++;
        }
    }

    // Se llama por cada fotograma en tiempo fijo
    private void FixedUpdate()
    {
        if (enable && !fantasma)
        {
            recogerInputs();
            procesarInputs();
        }
    }

    /// <summary>
    /// Recogemos los inputs
    /// </summary>
    private void recogerInputs()
    {
        // Recogemos del Input los valores de aceleración,giro y freno. 
        motor = maxMotorTorque * Input.GetAxis("Vertical");
        steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        brake = maxBrakeTorque * Input.GetAxis("Jump");
    }

    /// <summary>
    /// procesa los inputs y determina que acciones tomar segun sus valores
    /// </summary>
    private void procesarInputs()
    {
        Vector3 position;
        Quaternion rotation;
        for (int a = 0; a < colliderRuedas.Length; a++)
        {
            colliderRuedas[a].steerAngle = steering;
            colliderRuedas[a].motorTorque = motor;
            colliderRuedas[a].brakeTorque = brake;
            colliderRuedas[a].GetWorldPose(out position, out rotation);
            objetosRuedas[a].transform.position = position;
            objetosRuedas[a].transform.rotation = rotation;
        }
    }

    /// <summary>
    /// Comprueba los eventos de colision por trigger
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (!fantasma)
        {
            if (other.name.Contains("meta"))
            {
                Debug.Log("Se llega a la meta");
                //Se llega a meta
                EventoPista.metaVuelta();
            }
            else if (other.name.Contains("comprobacion"))
            {
                Debug.Log("Se llega a la comprobacion");
                //Se llega a comprobacion
                EventoPista.comprobadorVuelta();
            }
            else if (other.name.Contains("perder"))
            {
                //Se toca un objeto que hace perder la partida
                EventoPista.perder();
            }
        }
    }

    /// <summary>
    /// Hace transparente al fantasma
    /// </summary>
    public void convertirEnFantasma()
    {
        foreach(MeshRenderer mesh in mallas)
        {
            Material[] materiales = mesh.materials;

            if(materiales != null && materiales.Length > 0)
            foreach(Material mat in materiales)
            {
                if(mat != null)
                {
                    mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    mat.SetInt("_ZWrite", 0);
                    mat.DisableKeyword("_ALPHATEST_ON");
                    mat.DisableKeyword("_ALPHABLEND_ON");
                    mat.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    mat.renderQueue = 3000;
                    mat.color = new Color(0f, 0f, 1f, 0.8f);
                }
            }
        }
    }

}
