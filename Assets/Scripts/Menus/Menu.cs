using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Todos los eventos de presionar un boton en las UIs.
/// </summary>
public class Menu : MonoBehaviour
{
    
    public void iniciar()
    {
        SceneManager.LoadScene(EventoPista.PISTA);
    }

    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void reintentar()
    {
        SceneManager.LoadScene(EventoPista.PISTA);
    }

    public void salir()
    {
        Application.Quit(1);
    }

    public void seleccionarCoche(int coche)
    {
        EventoPista.COCHE = coche;
        avanzarPistas();
    }

    public void seleccionarPistas(string pista)
    {
        EventoPista.PISTA = pista;
        iniciar();
    }

    public void volverMenu()
    {
        ControladorMenu.goToMenu();
    }

    public void avanzarCoches()
    {
        ControladorMenu.goToCoches();
    }

    public void avanzarPistas()
    {
        ControladorMenu.goToMapas();
    }

    public void reproducir()
    {
        EventoPista.reproducir();
    }
}
