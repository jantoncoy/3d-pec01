using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorMenu : MonoBehaviour
{
    /// <summary>
    ///  Paneles
    /// </summary>
    public GameObject menu, coches, pistas;

    /// <summary>
    ///  evento que viaja hasta el menu
    /// </summary>
    public static Action goToMenu;
    /// <summary>
    ///  evento que navega hasta el panel de coches
    /// </summary>
    public static Action goToCoches;
    /// <summary>
    ///  evento que navega hasta el mapa
    /// </summary>
    public static Action goToMapas;

    // Start is called before the first frame update
    void Start()
    {
        ControladorMenu.goToMenu = volverMenu;
        ControladorMenu.goToCoches = avanzarCoches;
        ControladorMenu.goToMapas = avanzarPistas;
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    ///  Vuelve al menu
    /// </summary>
    public void volverMenu()
    {
        coches.SetActive(false);
        menu.SetActive(true);
        pistas.SetActive(false);
    }

    /// <summary>
    ///  Vuelve al menu de coches
    /// </summary>
    public void avanzarCoches()
    {
        coches.SetActive(true);
        menu.SetActive(false);
        pistas.SetActive(false);
    }

    /// <summary>
    ///  Vuelve al menu de pistas
    /// </summary>
    public void avanzarPistas()
    {
        coches.SetActive(false);
        menu.SetActive(false);
        pistas.SetActive(true);
    }

}
